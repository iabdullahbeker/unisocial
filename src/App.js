import React, { Component } from 'react';
import {BrowserRouter , Route} from 'react-router-dom';

import Header from './Component/Include/Header/header';
import Index from './Component/Index/index';
import Saved from './Component/Index/saved';
import Gropus from './Component/Groups/groups';
import Photos from './Component/Groups/includes/photos';
import Videos from './Component/Groups/includes/videos';
import Files from './Component/Groups/includes/files';
import Users from './Component/Users/users';
import Settings from './Component/Users/settings';
import Complaint from './Component/Index/Complaint';
import Question from './Component/Index/Question';
import './App.css';
import cookie from 'react-cookies';
import Login from './Component/Login/login';
import axios from 'axios';
import { messaging } from "./init-fcm";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


import HeaderLogin from './Component/Include/Header/header-login';
 
class App extends Component {

  state= {
    userId:null,
    user:[],
    notification:[]
  }
//this is life cycle

  componentWillMount() {
    this.setState({ userId: cookie.load('userId') });
    this.getInfoUser();
    // messaging.onMessage((payload) =>{
    //   let post_id =  payload.data['gcm.notification.post_id'];
    //   let msg =  payload.notification.body;
    //   let user_id =  payload.data['gcm.notification.user_id'];
    //   let yearandsection_id =  payload.data['gcm.notification.yearandsection_id'];
    //   let action =  payload.data['gcm.notification.action'];
    //   let name =  payload.data['gcm.notification.name'];

    //   this.add_notification(msg,post_id,user_id,yearandsection_id,action,name);
    // }); 
    messaging.onMessage((payload) => {
        // this.get_notification();

    });
    // navigator.serviceWorker.addEventListener("message", (message) => console.log(message));
  }

  // ...
async componentDidMount() {
  messaging.requestPermission()
    .then(async function() {
      const token = await messaging.getToken();
      console.log(token);
    })
    .catch(function(err) {
      console.log("Unable to get permission to notify.", err);
    });
  // navigator.serviceWorker.addEventListener("message", (message) => console.log(message));
  // this.get_notification();


}

/*
add_notification =(msg,post_id,user_id,yearandsection_id,action,name)=>{
  var data = new FormData();
  data.append('msg', msg);
  data.append('post_id', post_id);
  data.append('user_id', user_id);
  data.append('yearandsection_id', yearandsection_id);
  data.append('action', action);
  data.append('name', name);
  axios.post('http://localhost/fcm/add_notification.php',data)
  .then(res=>{
    toast.info("There is new Notification");
    this.get_notification();
  });

}
*/

/*
get_notification = () =>{
  var data = new FormData();
  data.append('user_id', cookie.load('userId'));
  data.append('yearandsection_id', cookie.load('year'));
  axios.post('http://localhost/fcm/get_notification.php',data)
  .then(res=>{
    console.log('Notification' , res.data);
    this.setState({
      notification:res.data
    })
  });
}
*/
  getInfoUser = () => {
    var id = cookie.load('userId');

    axios.get('http://uni-social.tk/api/v1/users/getById/'+id)
      .then(res => {
        this.setState({ user: res.data });
      })
  }

  onLogin =(userId,group,year,type,token)=> {
    //this.setState({ userId })
    cookie.save('userId', userId);
    cookie.save('group', group);
    cookie.save('year', year);
    cookie.save('type', type);
    this.getInfoUser();
    this.getCookie(token);

  }
 
  getCookie = (x)=>{
    var login = new FormData();
    login.append('tok', x);
      axios.post('http://uni-social.tk/esn/api/getC', login ).then(res => {
        if(res.data.status == 'true'){
        //  console.log(res.data);
          cookie.save('uid', res.data.uid);
          cookie.save('udat', res.data.udat);
          window.location = "/";
        }
  
      });
  }


  onLogout=()=> {
    cookie.remove('userId', { path: '/' });
    cookie.remove('group');
    cookie.remove('year');
    cookie.remove('type');
    cookie.remove('utoken');
    cookie.remove('uid');
    cookie.remove('udat');
    this.setState({
      userId:null
    })
  }

  render() {
    if(!this.state.userId){
      return(
        <React.Fragment>
              <HeaderLogin logout = {this.onLogout} />
              <Login onLogin = {this.onLogin}/>   
        </React.Fragment>       
       );
    }

    
    return (
      <BrowserRouter>
      <div className="App">
        <Header notification={this.state.notification}  logout = {this.onLogout} User={this.state.user}/>
                    <div className="home-r">
                    <div className="home-w">
                    <Route exact path="/users" component={Users} />
                    <Route exact path="/groups" component={Gropus} />
                    <Route exact path="/groups/photos" component={Photos} />
                    <Route exact path="/groups/videos" component={Videos} />
                    <Route exact path="/groups/files" component={Files} />
                    <Route exact path="/saved" component={Saved} />
                    <Route exact path="/complaint" component={Complaint} />
                    <Route exact path="/question" component={Question} />
                    <Route exact path="/settings" component={Settings} />
                    <Route exact path="/" component={Index}   />
                    </div> 
                    </div> 
        </div>
        
      </BrowserRouter>

    );
  }
}

export default App;
