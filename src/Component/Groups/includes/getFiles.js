import React, { Component } from 'react';
import '../../Include/GetPosts/getposts.css';
import '../css/Filtterposts.css';
import cookie from 'react-cookies';
import axios from 'axios';
import { toast } from 'react-toastify';
const $ = window.$;

class getFiles extends Component {

  constructor(props) {
    super(props);

    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.getPosts();
  }

  state = {
    userId: cookie.load('userId'),
    showloading2: false,
    ShowSubOpen2: false,
    ShowSubT2: false,
    numMaxPhotos: '',
    showModel: false,
    photosC: '',
    posts: [],
  
  }

  //*

  getPosts = () => {

    let checkboxes = '';
    let selectYear = '';
    let selectT = '';
    let p = this.props.Page
    

    this.setState({ getLoading: true });
    const data = { 'selectYear': selectYear, 'sec': checkboxes, 't': selectT, 'p': p, 'udat': cookie.load('udat'), 'uid': cookie.load('uid') };
    axios.post(`http://uni-social.tk/esn/api/getposts`, data)
      .then(res => {
        this.setState({ posts: res.data });
        this.setState({ getLoading: false });
      })
  }



  showImgs = (index, photos) => {
    if (index >= 0 && index < photos.length) {

      this.setState({ showModel: true });
      var previous = index - 1;
      var next = index + 1;
      var length = photos.length - 1;
      var photosC = (
        <div className="slidePhotos" id="slidePhotos" ref={this.setWrapperRef}>
          <div className="slidePhotos-c" id="slidePhotos">
            <div className="siledimg"> <img src={"http://uni-social.tk/files/posts/i-s/" + photos[index].fileName} /> </div>
          </div>
        </div>
      );

      this.setState({ photosC: photosC });
    }

  }

  showComments = (x) =>{
   console.log(x);
    $('#comments_'+x).show();  

  }


  CloseshowImgs = () => {
    this.setState({ showModel: false });

  }



  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }


  setWrapperRef(node) {
    this.wrapperRef = node;
  }


  handleClickOutside(event) {
    if (this.state.showModel && this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({ showModel: false });
    }
  }


  render() {




    var posts = '';
    posts = this.state.posts;
    var loading =
      <div className='timeline'>
        <div className='animated-bg'>
          <div className='p01'></div>
          <div className='p02'></div>
          <div className='p04'></div>
          <div className='p05'></div>
          <div className='p06'></div>
          <div className='p07'></div>
          <div className='p08'></div>
          <div className='p09'></div>
          <div className='p11'></div>
          <div className='p12'></div>
          <div className='p13'></div>
          <div className='p14'></div>
          <div className='fp15'></div>
        </div>
      </div>;

    return (
      <React.Fragment>



        <div className={'modelover ' + (this.state.showModel ? 'modelShow' : null)}>
          <div className="CloseSlidePhotos" id="CloseslidePhotos" onClick={this.CloseshowImgs}>x</div>


          {this.state.photosC}

        </div>

<div className="centerFiles">

{cookie.load('type') != 3 ?
this.state.getLoading ? loading :
          posts.map((post, index) => {
            return post.postStatus != 0 ?
            
              post.Photos[0] != '' ? (
                post.Photos.map((img, index) => (

                  img.fileType == 1 && this.props.Page === "photos" ? (

                  post.countPhotos >= 4 ?
                    this.state.numMaxPhotos = 4
                    :
                    this.state.numMaxPhotos = post.countPhotos,

                            <a onClick={() => { this.showImgs(index, post.Photos) }} className={'a-' + 1}>
                              <img src={'http://uni-social.tk/files/posts/i-s/' + img.fileName} />
                            </a>
                
                
                    ):

                    img.fileType == 2 && this.props.Page === "videos"  ? 
                    (
                    <div className={'showimg v' + post.Photos[0].filespace + ' num' + this.state.numMaxPhotos + ' i' + index}>
                    <div className={'imgs_' + this.state.numMaxPhotos}>
                    <a   className={'a-' + index}>

                    <video controls='1' loop='1'  >
                    <source src={'http://uni-social.tk/files/videos/' + img.fileName}  type='video/mp4' />
                    </video>

                    </a>
                    </div>
                    </div>
                   ):

                  img.fileType == 3 && this.props.Page === "files"  ? 
                  (
                  <div className={'showFiles' + post.Photos[0].filespace + ' num' + this.state.numMaxPhotos + ' i' + index}>
                  <div className={'showFiles-c'}>

                  <div className="shF-l">
                  <div className={'shF-ico ' + img.fileBType}></div>

                  <div className="shF-l-cc">
                  <div className="shF-name">{img.fileBName}</div>
                  <div className="shF-type" >{img.fileBType}</div>
                  </div>
                 </div>

                  <div className="shF-r">
                  <a target='_blank'  href={'http://uni-social.tk/files/files/'+img.fileName} >
                  <div className="shF-down-ico"></div>
                  </a>
                  </div>


                  </div>
                  </div>
                ):null


                  

                ))
              ) : null


     
                : null
          })
:null}



{/* -----------------------------------   sec*/}

{cookie.load('type') == 3 ?
this.state.getLoading ? loading :
          posts.map((post, index) => {
            return post.postStatus != 0 ?
              
                <div key={post.postId} className="post-h">
                  <div className="post">

                    <div className="p-user">
                      <div className="p-useri">
                        {
                          post.user[0].userImg != null ?
                            <img src={'http://uni-social.tk/files/uImg/i-ss/' + post.user[0].userImg} />
                            :
                            <img src="http://localhost/4up/ico/user0.jpg" />
                        }

                      </div>

                      <div className="box-name">
                        <div className="p-usern">{post.user[0].userName}</div>

                        <div className="p-usery">
                          <span className="p-usery1">{post.postTime}> </span>
                          <span className="p-usery2">{post.section[0].sectionName}.</span>
                        </div>
                      </div>
                      
                      {/*
          <Modal
                header='Post'
                fixedFooter
                
                >
                <Input type='textarea' defaultValue={post.content} rows="6" onChange={this.props.updateState} />
                <Row>
                <Button onClick ={()=>this.props.updatePost(post.postId)}>Update</Button>
                <Button onClick ={()=>this.props.deletePost(post.postId,index)}>Delete</Button>
                </Row>
          </Modal>
          */}
                    </div>

                    <div className="p-userp">
                      <pre>
                        {post.postContent}
                      </pre>
                    </div>

                    <div className="p-photos">
                      {
                        post.Photos[0] != '' ? (
                          post.Photos.map((img, index) => (
                            img.fileType == 1 ? (

                            post.countPhotos >= 4 ?
                              this.state.numMaxPhotos = 4
                              :
                              this.state.numMaxPhotos = post.countPhotos,

                            index <= 3 ?
                              (
                                post.countPhotos > 4 && index == 3 ?

                                  <div className={'moreimge ' + post.Photos[0].filespace}>
                                    <div className={'showimg ' + post.Photos[0].filespace + ' num' + this.state.numMaxPhotos + ' i' + index}>
                                      <div className={'imgs_' + this.state.numMaxPhotos}>
                                        <a onClick={() => { this.showImgs(index, post.Photos) }} className={'a-' + index}>
                                          <img src={'http://uni-social.tk/files/posts/i-s/' + img.fileName} />
                                          <div className="moreimge_count">+{post.countPhotos - 4}</div>

                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                  :

                                  <div className={'showimg ' + post.Photos[0].filespace + ' num' + this.state.numMaxPhotos + ' i' + index}>
                                    <div className={'imgs_' + this.state.numMaxPhotos}>
                                      <a onClick={() => { this.showImgs(index, post.Photos) }} className={'a-' + index}>
                                        <img src={'http://uni-social.tk/files/posts/i-s/' + img.fileName} />
                                      </a>
                                    </div>
                                  </div>

                              ) : null
                          
                          
                              ):

                              img.fileType == 2 ? 
                              (
                              <div className={'showimg v' + post.Photos[0].filespace + ' num' + this.state.numMaxPhotos + ' i' + index}>
                              <div className={'imgs_' + this.state.numMaxPhotos}>
                              <a   className={'a-' + index}>

                              <video controls='1' loop='1'  >
                              <source src={'http://uni-social.tk/files/videos/' + img.fileName}  type='video/mp4' />
                              </video>

                              </a>
                              </div>
                              </div>
                             ):

                            img.fileType == 3 ? 
                            (
                            <div className={'showFiles' + post.Photos[0].filespace + ' num' + this.state.numMaxPhotos + ' i' + index}>
                            <div className={'showFiles-c'}>

                            <div className="shF-l">
                            <div className={'shF-ico ' + img.fileBType}></div>

                            <div className="shF-l-cc">
                            <div className="shF-name">{img.fileBName}</div>
                            <div className="shF-type" >{img.fileBType}</div>
                            </div>
                           </div>

                            <div className="shF-r">
                            <a target='_blank'  href={'http://uni-social.tk/files/files/'+img.fileName} >
                            <div className="shF-down-ico"></div>
                            </a>
                            </div>


                            </div>
                            </div>
                          ):null


                            

                          ))
                        ) : null


                      }
                    </div>
 

                  </div>
 
                </div>

                : null
})
:null}

</div>


      </React.Fragment>

    );
  }

}




export default getFiles;

