import * as firebase from "firebase/app";
import "firebase/messaging";
const initializedFirebaseApp = firebase.initializeApp({
	// Project Settings => Add Firebase to your web app
  messagingSenderId: "623816298448"
});
const messaging = initializedFirebaseApp.messaging();
messaging.usePublicVapidKey(
	// Project Settings => Cloud Messaging => Web Push certificates
  "BKo6lfKRKuO-owFiYNErnDonzw8qsyWz2RytBHmBBTGJh9OX5Q_5yQJAutnWQM2ggnpmQsM23tqFTcaIoBiFwXg"
);
export { messaging };